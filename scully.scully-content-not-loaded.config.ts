import { ScullyConfig } from '@scullyio/scully';
export const config: ScullyConfig = {
  projectRoot: "./src",
  projectName: "scully-content-not-loaded",
  outDir: './dist/static',
  routes: {
    '/news/:id': {
      type: 'contentFolder',
      id: {
        folder: "./news"
      }
    },
    '/blog/:slug': {
      type: 'contentFolder',
      slug: {
        folder: "./blog"
      }
    },
  }
};